#!/bin/bash
DDJP="/etc/docker/daemon.json"

if [ ! -f "$DDJP" ]; then
    echo "daemon json file does not exist, copying it now"
    echo ""
    sudo cp ./docker_daemon.json "$DDJP"
    echo "restarting docker daemon now"
    echo ""
    sudo systemctl restart docker
    echo "docker daemon restarted"
    echo ""
else
    echo "daemon json file already exist, please consolidate it with the following content"
    cat ./docker_daemon.json
    echo ""
    echo "after doing that please run \"sudo systemctl restart docker\""
    echo ""
fi

echo "done"
echo ""