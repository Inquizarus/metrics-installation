#!/bin/bash

# Node exporter version desired
NEV="0.16.0"

# Store our current working directory for easier navigation
CWD=$(pwd)
echo "$CWD stored as current working directory"
echo ""

echo "moving over to tmp folder and downloading version $NEV of node_exporter"
echo ""
cd /tmp
curl -LO "https://github.com/prometheus/node_exporter/releases/download/v$NEV/node_exporter-$NEV.linux-amd64.tar.gz"

echo "extracting node_exporter binary"
echo ""
tar -xvf node_exporter-0.16.0.linux-amd64.tar.gz

echo "installing binary to /usr/local/bin"
echo ""
sudo mv node_exporter-0.16.0.linux-amd64/node_exporter /usr/local/bin/node_exporter

echo "moving back to $CWD"
echo ""
cd $CWD

echo "installing node_exporter service"
echo ""
sudo cp ./node_exporter.service /etc/systemd/system/

echo "creating user \"node_exporter\" for service unit to use"
echo ""
sudo useradd -rs /bin/false node_exporter

echo "reloading system daemon"
echo ""
sudo systemctl daemon-reload

echo "starting and enabling node_exporter service"
echo ""
sudo systemctl start node_exporter
sudo systemctl enable node_exporter