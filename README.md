
# Installation

Run `install.sh` in this directory to install both metrics for docker and the machine in general or go through each section separately to install the ones you want.
```bash
$ ./install.sh
```

## docker metrics

The docker daemon have a built in way of exposing information about itself and the containers that it runs. This mean that we can keep track of
how well containers run, if they die often or anything else.

---
Run `docker_daemon_metrics_insta.sh` in this directory and follow directions if any is presented.

```bash
$ ./docker_daemon_metrics_install.sh
```

### Curl to see if everything works and metrics are exposed
```bash
$ curl localhost:9323/metrics
```
#### Troubleshooting for docker metrics
##### Metrics can not be curled from outside the machine itself
_Make sure that port 9323 is open either for all incoming traffic or just for the ip that this data will be scraped from. Otherwise prometheus will be unable to scrape the metrics page._

```bash
$ sudo ufw status
...
9323                       ALLOW       Anywhere
9323 (v6)                  ALLOW       Anywhere (v6)
...
```
_If the port 9323 is not listed when running the command the it must be added with the following command (opens port for all incoming traffic)_
```bash
$ sudo ufw allow 9323
$ sudo ufw reload
```

## node_exporter

The node exporter exposes metrics about the machine itself, boot times, cpu loads and all that good stuff that might be desired to keep an eye on.
___

Run `node_exporter_install.sh` in this directory or follow the steps listed below for manual process for when the install script would not work.

```bash
$ ./node_exporter_install.sh
```
___
### Download tar of node_exporter
```bash
$ cd /tmp
$ curl -LO https://github.com/prometheus/node_exporter/releases/download/v0.16.0/node_exporter-0.16.0.linux-amd64.tar.gz
```
### Unpack the binary
```bash
$ tar -xvf node_exporter-0.16.0.linux-amd64.tar.gz
```
### Install binary
```bash
$ sudo mv node_exporter-0.16.0.linux-amd64/node_exporter /usr/local/bin/
```
### Copy `node_exporter.service` to `/etc/systemd/system/node_exporter.service`
```bash
$ sudo cp ./node_exporter.service /etc/systemd/system/
```
### Create node_exporter user for service to use
```bash
$ sudo useradd -rs /bin/false node_exporter
```
### Reload system daemon and start/enable new service
```bash
$ sudo systemctl daemon-reload
$ sudo systemctl start node_exporter
```

## Curl to see if everything works and metrics are exposed
```bash
$ curl localhost:9100/metrics
```

#### Troubleshooting for node_exporter
##### Metrics can not be curled from outside the machine itself
_Make sure that port 9100 is open either for all incoming traffic or just for the ip that this data will be scraped from. Otherwise prometheus will be unable to scrape the metrics page._

```bash
$ sudo ufw status
...
9100                       ALLOW       Anywhere
9100 (v6)                  ALLOW       Anywhere (v6)
...
```
_If the port 9100 is not listed when running the command the it must be added with the following command (opens port for all incoming traffic)_
```bash
$ sudo ufw allow 9100
$ sudo ufw reload
```